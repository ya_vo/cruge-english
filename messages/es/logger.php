<?php
/**
 * Spanish translation for Cruge Logger messages
 * @author Ricardo Obregón <ricardo@obregon.co>
 * @date 2/01/13 07:20 PM
 */
return array(
    'PERMISSION IS REQUIRED' => 'PERMISO REQUERIDO',
    'This page displays the roles, tasks and operations that are required by the current user but unassigned. This message is displayed because CrugeModule::rbacSetupEnabled = true' =>
    'Esta página le muestra los roles, Tasks y operations requeridas que el user actual no tiene asignadas. Este mensaje aparece porque CrugeModule::rbacSetupEnabled = true',
    'Assignments required by the user' => 'Permisos requeridos por el user',
    'Returned User' => 'user Retornado',
);