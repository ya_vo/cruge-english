<?php
/**
 * Cruge Admin messages
 * @author Ricardo Obregón <ricardo@obregon.co>
 * @date 4/12/12 06:11 PM
 */
return array(
    'User Manager' => 'Administrador de users',
    'Update Profile' => 'Editar profile',
    'This message is displayed because you have "debug" parameter enabled in the config file.' => 'Este mensaje se muestra porque tiene activo el parámetro "debug" en el archivo de configuración.',
    'Manage Users' => 'Administrar users',
    'Are you sure you want to delete this user' => '¿Está seguro de delete este user?',
    'Update User' => 'Editar user',
    'Create User' => 'Create user',
    'Delete User' => 'Delete user',
    '*** You are working as SuperAdministrator ***' => '*** Está operando como Súper Administrador ***',
    'Custom Fields' => 'Campos Personalizados',
    'List Profile Fields' => 'Listar Campos de profile',
    'Create Profile Field' => 'Create Field de profile',
    'Roles' => 'Roles',
    'Roles and Assignments' => 'Roles y Asignaciones',
    'Tasks' => 'Tasks',
    'Operations' => 'Operations',
    'Assign Roles to Users' => 'Assign Roles a users',
    'System' => 'Sistema',
    'Sessions' => 'Sessions',
    'System Variables' => 'Variables del Sistema',
    'Control Panel' => 'Panel de Control',
    'List Fields' => 'Listar Campos',
    'Create Field' => 'Create Field',
);