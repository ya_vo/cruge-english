<?php 
	/* formulario comun para create y update
	
		argumento:
		
		$model: instancia de CrugeAuthItemEditor
	*/
?>
<div class="form">
<?php $form = $this->beginWidget('CActiveForm', array(
    'id'=>'authitem-form',
    'enableAjaxValidation'=>false,
    'enableClientValidation'=>false,
)); ?>
<div class="row form-group">
	<div class='row'>
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>64,'maxlength'=>64)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
	<div class='row'>
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('size'=>50,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'description'); ?>
		<?php if($model->categoria  == "Task") { ?>
		<div class='hint'>Tip: use a ":" before a value for the Task is exported as a menuitem to use<br/> <span class='code'>
		Yii::app()->user->rbac->getMenu();</span> and close with a {Namemenuitem} to be within a -Namemenuitem-.
		Example: <span class='code'>":Edit your profile{menuprincipal}"</span></div>
		<?php } ?>
	</div>
	<div class='row'>
		<?php echo $form->labelEx($model,'businessRule'); ?>
		<?php echo $form->textField($model,'businessRule',array('size'=>50,'maxlength'=>512)); ?>
		<?php echo $form->error($model,'businessRule'); ?>
		<p class='hint'>
			<?php echo CrugeTranslator::t("defines a business rule that will be executed each time the item is evaluated by calling checkAccess, the argument params is delivered optionally checkAccess:"); ?>
			<br/>
			<?php echo CrugeTranslator::t(
			"Example:"); ?>
			<br/>
			<div class='code'>return Yii::app()->user->id==$params["post"]->authID;</div>
			<br/>
			<div class='code'>
				$params = ...<?php echo CrugeTranslator::t("anything"); ?>...;<br/>
				if(Yii::app()->user->checkAccess('<?php echo $model->name;?>', $params)){ ... }
			</div>
			<br/>
		</p>
	</div>
</div>

<div class="row buttons">
	
	<?php Yii::app()->user->ui->tbutton(($model->isNewRecord ? 'Create New' : 'Update')); ?>
	<?php Yii::app()->user->ui->bbutton("Cancel",'Cancel'); ?>
</div>
<?php echo $form->errorSummary($model); ?>
<?php $this->endWidget(); ?>
</div>
