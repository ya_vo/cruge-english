<h2>Welcome!</h2>
<p>Dear user, this email was sent because you have created a new account for
you in the system, please note:</p>
<b>Username and Password</b>
<p><?php echo "user: ".$model->username;?></p>
<p><?php echo "email: ".$model->email;?></p>
<p><?php echo "password: ".$password;?></p>

<h3>You must activate your account:</h3>
<p>Please follow this link to activate your account:</p>
<p><?php echo Yii::app()->user->um->getActivationUrl($model); ?>
</p>
<p>Please take precautions to keep this information.</p>