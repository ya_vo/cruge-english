CRUGE
-----
Extension to control users and Roles.

##What is Cruge ?

Cruge is an Advanced Extension of the API for user and role control of the Yii Framework. It incorporates:

* visual editor of Roles, Tasks and Operations, with AJAX
* registration, login and lost password functions
* advanced management of users
* customizable profile fields
* advanced customisable view emails



Cruge has an advanced high to low-level API. It's not just a simple extension. There are advanced OOP controls for sessions, management of users and events and more.

---
How to install?: 
---
[install instructions](https://bitbucket.org/ya_vo/cruge-english/wiki/Installation "install instructions")

---
Tutorial on how to use
---
[tutiral here](https://bitbucket.org/ya_vo/cruge-english/wiki/Cruge%20tutorial "tutorial here")

-----
I leave below the original links to the Spanish version.
-----

[Documentación en Línea (WIKI)](http://yiiframeworkenespanol.org/cruge/ "Documentación en Línea (WIKI)")

[visita mi blog](http://trucosdeprogramacionmovil.blogspot.com/ "Visita mi Blog")

[Comunidad de Yii Framework en Español](http://yiiframeworkenespanol.org/ "Yii Framework en Español")

[PROYECTO BASICO DE DEMOSTRACION](https://bitbucket.org/christiansalazarh/crugeholamundo/ "Cruge Demo")

![screenshots][1]

##Requisitos de Plataforma

* PHP Version 5.2.6, 5.4.4, 5.4.5

* Yii Framework 1.10,  1.11, 1.12, 1.13

##Licencia

Ver archivo adjunto LICENSE. Free BSD License.

##Instalación

 Por favor ve a sitio WIKI de documentación en línea para que conozcas mas detalles de como instalar.

##DOCUMENTACION

 [Documentación en Línea (WIKI)](http://yiiframeworkenespanol.org/cruge/ "Documentación en Línea (WIKI)")

[1]: https://bitbucket.org/christiansalazarh/cruge/downloads/screenshots.gif
